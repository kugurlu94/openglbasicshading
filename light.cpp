#include "light.h"

/* LightProperties Implementation */

const color4 LightProperties::DEFAULT_AMBIENT = color4( 1.0, 1.0, 1.0, 1.0 );
const color4 LightProperties::DEFAULT_DIFFUSE = color4( 1.0, 1.0, 1.0, 1.0 );
const color4 LightProperties::DEFAULT_SPECULAR = color4( 1.0, 1.0, 1.0, 1.0 );


LightProperties::LightProperties(const color4& ambient_c, const color4& diffuse_c, const color4& specular_c){
    this->ambient = ambient_c;
    this->diffuse = diffuse_c;
    this->specular = specular_c;
}

color4& LightProperties::getAmbient(){
    return this->ambient;
}

color4& LightProperties::getDiffuse(){
    return this->diffuse;
}

color4& LightProperties::getSpecular(){
    return this->specular;
}


/* PeflectivityPorperties Implementation */

const color4 ReflectivityProperties::DEFAULT_AMBIENT = color4( 1.0, 1.0, 1.0, 1.0 );
const color4 ReflectivityProperties::DEFAULT_DIFFUSE = color4( 1.0, 1.0, 1.0, 1.0 );
const color4 ReflectivityProperties::DEFAULT_SPECULAR = color4( 1.0, 1.0, 1.0, 1.0 );
const color4 ReflectivityProperties::DEFAULT_EMISSION = color4( 0.0, 0.0, 0.0, 1.0 );
const GLfloat ReflectivityProperties::DEFAULT_SHININESS = 100.0;


ReflectivityProperties::ReflectivityProperties(const color4& ambient_c, const color4& diffuse_c, const color4& specular_c, const color4& emission, const GLfloat& shininess){
    this->ambient = ambient_c;
    this->diffuse = diffuse_c;
    this->specular = specular_c;
    this->emission = emission;
    this->shininess = shininess;
}

color4& ReflectivityProperties::getAmbient(){
    return this->ambient;
}

color4& ReflectivityProperties::getDiffuse(){
    return this->diffuse;
}

color4& ReflectivityProperties::getSpecular(){
    return this->specular;
}

color4& ReflectivityProperties::getEmission(){
    return this->emission;
}

GLfloat& ReflectivityProperties::getShininess(){
    return this->shininess;
}

void ReflectivityProperties::setShininess(const GLfloat& mat_shininess){
    this->shininess = mat_shininess;
}


/* Implementation of LightSource */ 

LightSource::LightSource(const point4& l_position, const LightProperties& l_properties){
    this->position = l_position;
    this->properties = l_properties;
}


LightSource::LightSource(const point4& l_position){
    this->position = l_position;
}


LightSource::LightSource(){}


point4& LightSource::getPosition(){
    return this->position;
}


LightProperties& LightSource::getProperties(){
    return this->properties;
}

void LightSource::setPosition(const point4& l_position){
    this->position = l_position;
}


/* Implementation of LightingManager */

LightingManager::LightingManager(const GLuint& program){
    this->program = program;
    this->enabledLocation = glGetUniformLocation(this->program, "lightSources[0].enabled");
    this->ambientLocation = glGetUniformLocation(this->program, "lightSources[0].ambient");
    this->diffuseLocation = glGetUniformLocation(this->program, "lightSources[0].diffuse");
    this->specularLocation = glGetUniformLocation(this->program, "lightSources[0].specular");
    this->positionLocation = glGetUniformLocation(this->program, "lightSources[0].position");
    this->locationOffset = 5;
}

/* Inserts and enables a light source*/ 
GLuint LightingManager::addLightSource(const LightSource& l_source){
    GLuint source_id;
    this->lightSources.push_back(l_source);
    this->lightStatuses.push_back(false);
    
    source_id = this->lightSources.size() - 1;
    this->allocateLightSource(source_id);
    this->enableLightSource(source_id);

    return source_id;
}


void LightingManager::allocateLightSource(const GLuint& source_id){
    if (source_id < this->lightSources.size()){
        LightSource& l_source = this->lightSources[source_id];
        LightProperties& l_properties = l_source.getProperties();
        GLuint offset = source_id * this->locationOffset;

        glUseProgram(program);
        glUniform4fv(this->ambientLocation + offset, 1, l_properties.getAmbient());
        glUniform4fv(this->diffuseLocation + offset, 1, l_properties.getDiffuse());
        glUniform4fv(this->specularLocation + offset, 1, l_properties.getSpecular());
        glUniform4fv(this->positionLocation + offset, 1, l_source.getPosition());
        glUseProgram(0);
    }
}

/* 
    Enables the current light source with given source_id
    Updates uniform variables in the given program.
*/

void LightingManager::enableLightSource(const GLuint& source_id){
    if (source_id < this->lightSources.size()){
        GLuint offset = source_id * this->locationOffset;

        this->lightStatuses[source_id] = true;

        glUseProgram(program);
        glUniform1ui(this->enabledLocation + offset, 1);
        glUseProgram(0);
    }
}


void LightingManager::disableLightSource(const GLuint& source_id){
    if (source_id < this->lightSources.size()){
        GLuint offset = source_id * this->locationOffset;

        this->lightStatuses[source_id] = false;

        glUseProgram(program);
        glUniform1ui(this->enabledLocation + offset, 0);
        glUseProgram(0);
    }
}

bool LightingManager::lightEnabled(const GLuint& source_id){
    if (source_id < this->lightSources.size())
        return this->lightStatuses[source_id];
    else
        return false;
}

void LightingManager::setPosition(const GLuint& source_id, const point4& position){
    if (source_id < this->lightSources.size()){
        this->lightSources[source_id].setPosition(position);
        this->allocateLightSource(source_id);
    }
}