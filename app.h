#ifndef __APP_H
#define __APP_H

#include "absapp.h"
#include "obj.h"
#include "camera.h"
#include "light.h"

/* Possible colors. */
enum color_t {c_black, c_red, c_yellow, c_green, c_blue, c_magenta, c_white, c_cyan, c_gray};

/* Draw modes. */
enum draw_mode_t {WIREFRAME_MODE = 0, SHADING_MODE = 1, TEXTURE_MODE = 2, TEXTURE_SHADING_MODE = 3};

/* Shading modes. */
enum shading_mode_t {GOURAUD_SHADING = 0, PHONG_SHADING = 1};

/* Reflection models */
enum reflection_model_t {PHONG_MODEL = 0, MODIFIED_PHONG_MODEL = 1};

/* Projection modes. */
enum proj_mode_t {ORTHOGRAPHIC_MODE, PERSPECTIVE_MODE};

/* Material types */
enum material_t {PLASTIC, METALIC};

class ViewerApplication : virtual public Application {
    private:
        friend void changeBackgroudColorCallback(int value);
        friend void changeColorCallback(int value);
        friend void changeDrawModeCallback(int value);
        friend void changeLightsBoundCallback(int value);
        friend void changeMaterialCallback(int value);
        friend void changeProjectionModeCallback(int value);
        friend void changeReflectionModelCallback(int value);
        friend void changeShadingModeCallback(int value);
        friend void switchLightOnOffCallback(int value);

    protected:
        /* Amount of zoom for each ket event. */
        static const GLfloat ZOOM_DELTA;

        /* Amount of rotation for each key event. */
        static const GLfloat ROTATION_DELTA;

        /* Default color */
        static const color_t DEFAULT_COLOR;

        /* Default draw mode */
        static const draw_mode_t DEFAULT_DRAW_MODE;

        /* Default shading mode */
        static const shading_mode_t DEFAULT_SHADING_MODE;
        
        /* Default reflection model */
        static const reflection_model_t DEFAULT_REFLECTION_MODEL;

        /* Default material */
        static const material_t DEFAULT_MATERIAL;

        /* Default Projection mode */
        static const proj_mode_t DEFAULT_PROJECTION_MODE;

        /* Default lights bound */
        static const bool DEFAULT_LIGHTS_BOUND;

        /* Light directions */
        static const point3 POINT_SOURCE_VECTOR;
        static const point3 DIRECTIONAL_SOURCE_VECTOR;

        /* Default point source distance */
        static const GLfloat DEFAULT_POINT_SOURCE_DISTANCE;

        /* Min point source distance */
        static const GLfloat MIN_POINT_SOURCE_DISTANCE;

        /* Point source distance step */
        static const GLfloat POINT_SOURCE_DISTANCE_STEP;

        /* uniform attribute name for color */
        static const char* COLOR_ATTRIB_NAME;

        /* uniform attribute name for draw mode */
        static const char* DRAW_MODE_ATTRIB_NAME;

        /* uniform attribute name for shading mode */
        static const char* SHADING_MODE_ATTRIB_NAME;

        /* uniform attribute name for reflection model */
        static const char* REFLECTION_MODEL_ATTRIB_NAME;

        /* uniform attribute name for LightModelMatrix */
        static const char* LIGHT_MODEL_MATRIX_NAME;

        /* uniform location for color attribute */
        GLuint colorLocation;

        /* uniform location for draw mode attribute */
        GLuint drawModeLocation;

        /* uniform location for shading mode */
        GLuint shadingModeLocation;

        /* uniform location for reflection model */
        GLuint reflectionModelLocation;

        /* uniform location for light model Matrix */
        GLuint lightModelMatrixLocation;

        /* transformation matrix for lights */
        mat4 lightModelMatrix;

        /* current color */
        color_t currentColor;

        /* current projection mode */
        proj_mode_t projectionMode;

        /* Pop-up menu handles */
        int mainMenuHandle;
        int changeDrawModeMenuHandle;
        int changeColorMenuHandle;
        int changeBackgroudColorMenuHandle;
        int changeLightsBoundMenuHandle;
        int changeMaterialMenuHandle;
        int changeProjectionMenuHandle;
        int changeReflectionModelMenuHandle;
        int changeShadingModeMenuHandle;
        int switchLightMenuHandle;
        

        /* Variable storing the object state. */
        GLObjectState * o_state = nullptr; // object state contains the rotation and zoom values for both objects.
        GLObject * c_object = nullptr; // current object on display

        /* Camera */
        Camera * camera = nullptr;

        /* Lighting Manager */
        LightingManager * lightingManager = nullptr;
        
        /* Light state */
        GLObjectState * l_state = nullptr; // state containg the rotation of lights

        /* Indicator for light bounds */
        bool lightsBound;

        /* Light ids */
        GLuint pointSourceId;
        GLuint directionalSourceId;

        /* Point source distance */
        GLfloat pointSourceDistance;

        /* The amount camera is moved away from the object in the center */
        const GLfloat cameraDistance = 4.0;

        /* Methods to update matrices */
        virtual void updateLightModelMatrix();
        virtual void updateModelMatrix();
        virtual void updateViewMatrix();
        virtual void updateProjectionMatrix();

        void changeBackgroudColor(const color_t& color);
        void changeColor(const color_t& color);
        void changeDrawMode(const draw_mode_t& mode);
        void changeLightsBound(const bool is_bound);
        void changeMaterial(const material_t& material);
        void changePointSourceDistance(const GLfloat& distance);
        void changeProjectionMode(const proj_mode_t& mode);
        void changeReflectionModel(const reflection_model_t& model);
        void changeShadingMode(const shading_mode_t& mode);
        void switchLightOnOff(const GLuint& source_id);

        void initMenus();

        virtual void keyControl(unsigned char key, int x, int y);
        virtual void render();
        virtual void resize(int width, int height);
        virtual void specialKeyControl(int key, int x, int y);

    public:
        explicit ViewerApplication(const std::string& title, const int width, const int height);
        virtual void init(int argc, char** argv);
};
#endif