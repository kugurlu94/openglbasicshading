#version 450 

const uint MAX_LIGHT_SOURCES = 2;

in vec4 shadedColor;
in vec2 texCoord;

/* Phong shading attributes*/
in vec3 phongN;
in vec3 phongE;
in vec3 phongL[MAX_LIGHT_SOURCES];

out vec4 color;

struct LightSource{
    uint enabled;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    vec4 position;
};

struct Material {
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float shininess;
};

uniform uint DrawMode; // Draw mode 0 for wireframe, 1 for shading, 2 for texture
uniform uint ShadingMode; // Shading mode 0 for gouraud, 1 for phong
uniform uint ReflectionModel; // Reflection mode 0 for phong, 1 for modified phong

uniform mat4 ModelMatrix;
uniform mat4 ViewMatrix;
uniform mat4 LightModelMatrix; // Model matrix for lights.

uniform LightSource lightSources[MAX_LIGHT_SOURCES];
uniform Material material;

uniform sampler2D texSampler;

void main() 
{
    if((DrawMode == 1 || DrawMode == 3) && ShadingMode == 1){ // phong shading
        vec4 mat_ambient;
        vec4 mat_diffuse;
        vec4 mat_specular;
        float mat_shininess;

        // Override material properties specially for texture shading mode.
        // I chose shades of grey specially to keep colors of the shader.
        if(DrawMode == 3){
            mat_ambient = vec4(0.1, 0.1, 0.1, 1.0);
            mat_diffuse = vec4(1.0, 1.0, 1.0, 1.0);
            mat_specular = vec4(0.6, 0.6, 0.6, 1.0);
            mat_shininess = material.shininess;
        } else {
            mat_ambient = material.ambient;
            mat_diffuse = material.diffuse;
            mat_specular = material.specular;
            mat_shininess = material.shininess;
        }

        // Loop adds up color contributions for each light source
        uint i;
        for(i=0; i < MAX_LIGHT_SOURCES; i++){
            if (lightSources[i].enabled > 0){
                // Compute terms in the illumination equation
                vec4 ambient = lightSources[i].ambient * mat_ambient;
                float d_comp = 1.0;

                vec3 N = normalize(phongN);
                vec3 E = normalize(phongE);
                vec3 L = normalize(phongL[i]);

                if(lightSources[i].position.w > 0.0){ // point source, add distance term
                    vec3 light_pos3 = (ViewMatrix * LightModelMatrix * lightSources[i].position).xyz;
                    d_comp = 10 / pow(distance(light_pos3, -phongE), 2);
                }

                float Kd = d_comp * max( dot(L, N), 0.0 ); //set diffuse to 0 if light is behind the surface point
                vec4  diffuse = Kd * lightSources[i].diffuse * mat_diffuse;

                float Ks;
                if (ReflectionModel == 1){ // modified phong.
                    vec3 H = normalize( L + E ); // halfway vector
                    Ks = d_comp * pow( max(dot(N, H), 0.0), mat_shininess );
                } else {
                    vec3 R = normalize(2*(dot(L, N)*N) - L);
                    Ks = d_comp * pow( max(dot(R, E), 0.0), mat_shininess );
                }
                vec4  specular = Ks * lightSources[i].specular * mat_specular;
                
                //ignore also specular component if light is behind the surface point
                if( dot(L, N) < 0.0 ) {
                    specular = vec4(0.0, 0.0, 0.0, 1.0);
                }
                
                if(DrawMode == 3) // texture shading
                    color += (ambient + diffuse + specular) * texture( texSampler, texCoord );
                else
                    color += ambient + diffuse + specular;             
            }
        }
    } else if(DrawMode == 2){ // Texture mode
        color = texture( texSampler, texCoord );
    }
    else{ // Color already calculated at vertex. Wireframe or Gouraud
        if (DrawMode == 3) // Texture shading (must be gouraud shading)
            color = shadedColor * texture( texSampler, texCoord );
        else
            color = shadedColor;
    }
}