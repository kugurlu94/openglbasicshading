CC = g++
CPPVERSION = -std=c++11
DEBUG = -O2 -g
LDLIBS = -lglut -lGLEW -lGL -lGLU -lpthread

CXXINCS = -I include
COMMON_DIR = common

INIT_SHADER = $(COMMON_DIR)/InitShader.cpp
INIT_SHADER_OBJ = $(COMMON_DIR)/InitShader.o

ABSAPP_H = absapp.h
ABSAPP = absapp.cpp
ABSAPP_OBJ = absapp.o

APP_H = app.h
APP = app.cpp
APP_OBJ = app.o

OBJ_H = obj.h
OBJ = obj.cpp
OBJ_OBJ = obj.o

CAMERA_H = camera.h
CAMERA = camera.cpp
CAMERA_OBJ = camera.o

LIGHT_H = light.h
LIGHT = light.cpp
LIGHT_OBJ = light.o

MAIN_CPP = main.cpp

TARGETS = main.out

all: $(TARGETS)

.PHONY: clean cleanall

$(TARGETS): $(LIGHT_OBJ) $(CAMERA_OBJ) $(OBJ_OBJ) $(APP_OBJ) $(ABSAPP_OBJ) $(INIT_SHADER_OBJ) $(MAIN_CPP)
	$(CC) $(CPPVERSION) $(DEBUG) $(CXXINCS) $(INIT_SHADER_OBJ) $(ABSAPP_OBJ) $(APP_OBJ) $(OBJ_OBJ) $(CAMERA_OBJ) $(LIGHT_OBJ) $(MAIN_CPP) $(LDLIBS) -o $@

$(LIGHT_OBJ): $(LIGHT_H) $(LIGHT)
	$(CC) $(CPPVERSION) -c -Wall $(CXXINCS) $(LIGHT) -o $(LIGHT_OBJ)

$(CAMERA_OBJ): $(CAMERA_H) $(CAMERA)
	$(CC) $(CPPVERSION) -c -Wall $(CXXINCS) $(CAMERA) -o $(CAMERA_OBJ)

$(OBJ_OBJ): $(OBJ_H) $(OBJ)
	$(CC) $(CPPVERSION) -c -Wall $(CXXINCS) $(OBJ) -o $(OBJ_OBJ)

$(APP_OBJ): $(APP_H) $(APP)
	$(CC) $(CPPVERSION) -c -Wall $(CXXINCS) $(APP) -o $(APP_OBJ)

$(ABSAPP_OBJ): $(ABSAPP_H) $(ABSAPP) $(INIT_SHADER_OBJ)
	$(CC) $(CPPVERSION) -c -Wall $(CXXINCS) $(ABSAPP) -o $(ABSAPP_OBJ)

$(INIT_SHADER_OBJ): $(INIT_SHADER)
	$(CC) $(CPPVERSION) -c -Wall $(INIT_SHADER) -o $(INIT_SHADER_OBJ)

clean:
	\rm $(TARGETS)

cleanall:
	\rm $(INIT_SHADER_OBJ) $(ABSAPP_OBJ) $(APP_OBJ) $(OBJ_OBJ) $(CAMERA_OBJ) $(LIGHT_OBJ) $(TARGETS)
