#ifndef __OBJ_H
#define __OBJ_H

#include <Angel.h>
#include "light.h"


typedef Angel::vec4  color4;
typedef Angel::vec4  point4;
typedef Angel::vec3  point3;
typedef Angel::vec2  point2;

// Class that describes a objects state.
// Genereates the model matrix.
class GLObjectState {
    protected:
        // Initial state variables for recovery.
        GLfloat init_x;
        GLfloat init_y;
        GLfloat init_z;
        GLfloat init_scale;

        // State variables to generate ModelViewMatrix
        mat4 rotateMatrix;
        GLfloat scale;
    public:
        static const GLfloat DEFAULT_X;
        static const GLfloat DEFAULT_Y;
        static const GLfloat DEFAULT_Z;
        static const GLfloat DEFAULT_SCALE;
        static const GLfloat MIN_SCALE;
        static const GLfloat MAX_SCALE;

        // Explicit constructor.
        explicit GLObjectState(
            const GLfloat& x_rot = DEFAULT_X,
            const GLfloat& y_rot = DEFAULT_Y, 
            const GLfloat& z_rot = DEFAULT_Z,
            const GLfloat& scale = DEFAULT_SCALE);
        
        // rotation methods
        void rotateX(const GLfloat& rot);
        void rotateY(const GLfloat& rot);
        void rotateZ(const GLfloat& rot);

        // zoom methods
        void zoomIn(const GLfloat& val);
        void zoomOut(const GLfloat& val);

        // reset methods
        void resetRotation();
        void reset();

        mat4 getModelMatrix();
};

// abstract GLObject used for abstracting the details of
// each type of object to draw
class GLObject {
    protected:
        static const char* VERTEX_POSITION_ATTRIB_NAME; // Vertex position attribute name
        static const char* TEXTURE_COORDINATE_ATTRIB_NAME; // texture coordinate attribute name
        static const char* NORMAL_POSITION_ATTRIB_NAME; // Normal attribute name.

        GLuint program; // program object belongs to.
        GLuint positionAttrib; // position attrib location.
        GLuint texCoordAttrib; // texture attrib location.
        GLuint normalAttrib; // normal attrib location.
        
        /* Material attrib locations */
        GLuint ambientAttribLocation;
        GLuint diffuseAttribLocation;
        GLuint specularAttribLocation;
        GLuint shininessAttribLocation;

        bool allocated; // whether or not obj is allocated on gpu.

        ReflectivityProperties reflectivityProperties;

        void setReflectivityAttribLocations();
        void setReflectivityAttribs();
    public:
        explicit GLObject(GLuint program);
        bool isAllocated();
        virtual void allocate() =0; // method for allocating memory in gpu
        virtual void deallocate(){}; // method for deallocating memory in gpu
        virtual void draw() =0; // method for drawing the pre-allocated object

        void setReflectivityProperties(const ReflectivityProperties& r_properties);
        virtual void setColor(const color4& color);
        virtual void setShininess(const GLfloat& shininess);
};

// Simple cube class for ease of testing.
class GLCube : public GLObject {
    protected:
        GLuint vao_id; // vertex array object for storing vertex data.
        GLuint vbo_ids[2]; // vertex buffer objects. index 0 for vertex data, index 1 for index data.

        // Vertices
        point4 vertices[8] = {
            point4(-.5f, -.5f,  .5f, 1),
            point4(-.5f,  .5f,  .5f, 1),
            point4( .5f,  .5f,  .5f, 1),
            point4( .5f, -.5f,  .5f, 1),
            point4(-.5f, -.5f, -.5f, 1),
            point4(-.5f,  .5f, -.5f, 1),
            point4( .5f,  .5f, -.5f, 1),
            point4( .5f, -.5f, -.5f, 1),
        };

        GLuint indices[36] = {
            0,2,1,  0,3,2,
            4,3,0,  4,7,3,
            4,1,5,  4,0,1,
            3,6,2,  3,7,6,
            1,6,5,  1,2,6,
            7,5,6,  7,4,5
        };

    public:
        explicit GLCube(GLuint program);
        virtual void allocate();
        virtual void draw();

        void allocateNormals();
};


// Object which reads the vertex data from a OFF file.
class GLOffObject : public GLObject {
    protected:
        GLuint vao_id; // vertex array object for storing vertex data.
        GLuint vbo_ids[2]; // vertex buffer objects. index 0 for vertex data, index 1 for index data.

        int numVertices; // number of vertices.
        point4* vertices;
        int numIndices; // number of indices.
        GLuint* indices;
    public:
        explicit GLOffObject(GLuint program, std::string filename);
        virtual void allocate();
        virtual void draw();
};

// Object which reads the vertex data from a OFFX file.
class GLOffXObject : public GLObject {
    protected:
        GLuint vao_id; // vertex array object for storing vertex data.
        GLuint vbo_ids[2]; // vertex buffer objects. index 0 for vertex data, index 1 for normal data and index 2 for index data.
        GLuint tex_id; // id for texture.

        int numVertices; // number of vertices.
        point4* vertices;
        
        int numTexCoords; // number of texture coordinates
        point2* texCoords;

        int numNormals; // number of normals.
        point3* normals;

        int numIndices; // number of indices.
        GLuint* indices;

        void parseOffx(std::string filename);

        // texture image length
        int textureRowLength;
        int textureColumnLength;
        GLubyte * textureImage;
        void parseImage(std::string imageFilename);
    public:
        explicit GLOffXObject(GLuint program, std::string filename, std::string textureFilename);
        virtual void allocate();
        virtual void draw();
};



#endif