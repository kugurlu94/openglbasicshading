#ifndef __CAMERA_H
#define __CAMERA_H

#include <Angel.h>

typedef Angel::vec4  point4;
typedef Angel::vec4  vec4;

class Camera {
    protected:
        /* Location of the eye */
        point4 eye;

        /* Direction of the look */
        vec4 at;

        /* Up vector */
        vec4 up;

    public:
        /* Explicit constructors */
        explicit Camera(const point4& eye, const vec4& at, const vec4& up);
        explicit Camera(const GLfloat& eye_x, const GLfloat& eye_y, const GLfloat& eye_z);
        explicit Camera();

        /* Methods to move camera around */
        void moveX(const GLfloat& delta_x);
        void moveY(const GLfloat& delta_y);
        void moveZ(const GLfloat& delta_z);
        void move(const vec4& move_vec);
        
        /* method generating view matrix */
        mat4 getViewMatrix();
};
#endif