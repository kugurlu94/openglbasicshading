#include "absapp.h"

/* Single to pointer to current Application */
Application * Application::currentApplication = nullptr;

/*
    Callback functions to attach for events.
*/
void displayCallback(){
    if(Application::currentApplication != nullptr)
        Application::currentApplication->render();
}

void resizeCallback(int width, int height){
    if(Application::currentApplication != nullptr)
        Application::currentApplication->resize(width, height);
}

void keyboardCallback(unsigned char key, int x, int y){
    if(Application::currentApplication != nullptr)
        Application::currentApplication->keyControl(key, x, y);
}

void specialCallBack(int key, int x, int y){
    if(Application::currentApplication != nullptr)
        Application::currentApplication->specialKeyControl(key, x, y);
}

void mouseCallback(int button, int state, int x, int y){
    if(Application::currentApplication != nullptr)
        Application::currentApplication->mouseControl(button, state, x,  y);
}


/* Static member variables */
const char* Application::VERTEX_SHADER =  "vshader.glsl";
const char* Application::FRAGMENT_SHADER = "fshader.glsl";
const char* Application::MODEL_MATRIX_NAME = "ModelMatrix";
const char* Application::VIEW_MATRIX_NAME = "ViewMatrix";
const char* Application::PROJECTION_MATRIX_NAME = "ProjectionMatrix";


/* Constructor */
Application::Application(const string& title, const int w_width, const int w_height){
    this->windowTitle = title;
    this->width = w_width;
    this->height = w_height;
}

/* Set default OpenGL configurations */
void Application::glConf(){
    // Default OpenGL configurations.
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glDepthFunc(GL_LESS);
    glFrontFace(GL_CCW);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
}

/* set clear color */
void Application::setClearColor(const color4& color){
    glClearColor(color.x, color.y, color.z, color.w);
    glutPostRedisplay();
}

void Application::init(int argc, char* argv[]){
    // Initilizes the OpenGL context for the application
    // Global glut options.
    glutInit(&argc, argv);
    glutInitContextVersion(4, 5); // I used this version in my system.
    glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
    glutInitContextProfile(GLUT_CORE_PROFILE);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    glutSetOption(
        GLUT_ACTION_ON_WINDOW_CLOSE,
        GLUT_ACTION_GLUTMAINLOOP_RETURNS
    );

    glutInitWindowSize(this->width, this->height);
    this->windowHandle = glutCreateWindow(windowTitle.c_str());

        if(windowHandle < 1) {
        cerr << "Cannot create window." << endl;
        exit(EXIT_FAILURE);
    }

    glewExperimental = GL_TRUE;
    GLenum GlewInitResult = glewInit();

    if (GLEW_OK != GlewInitResult) {
        cerr << "ERROR: " << glewGetErrorString(GlewInitResult) << endl;
        exit(EXIT_FAILURE);
    }
    glGetError(); // glew creates a error for some reason.

    this->glConf();

    // Compiling shaders and generating program
    this->program = InitShader(VERTEX_SHADER, FRAGMENT_SHADER);

    // Get uniform variable locations
    glUseProgram(program);
    this->modelMatrixLocation = glGetUniformLocation(program, MODEL_MATRIX_NAME);
    this->viewMatrixLocation = glGetUniformLocation(program, VIEW_MATRIX_NAME);
    this->projectionMatrixLocation = glGetUniformLocation(program, PROJECTION_MATRIX_NAME);
    glUseProgram(0);

    Application::currentApplication = this;
}

void Application::start(){
    // Set Uniform matrices
    this->updateModelMatrix();
    this->updateViewMatrix();
    this->updateProjectionMatrix();

    /* Attach callbacks */
    glutDisplayFunc(displayCallback);
    glutReshapeFunc(resizeCallback);
    glutKeyboardFunc(keyboardCallback);
    glutSpecialFunc(specialCallBack);
    glutMouseFunc(mouseCallback);

    glutPostRedisplay();
    glutMainLoop();
}
