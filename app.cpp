#include "app.h"

void changeBackgroudColorCallback(int value){
    if(ViewerApplication::currentApplication != nullptr){
        dynamic_cast<ViewerApplication *>(ViewerApplication::currentApplication)->changeBackgroudColor(color_t(value));
    }
}

void changeColorCallback(int value){
    if(ViewerApplication::currentApplication != nullptr){
        dynamic_cast<ViewerApplication *>(ViewerApplication::currentApplication)->changeColor(color_t(value));
    }
}

void changeDrawModeCallback(int value){
    if(ViewerApplication::currentApplication != nullptr){
        dynamic_cast<ViewerApplication *>(ViewerApplication::currentApplication)->changeDrawMode(draw_mode_t(value));
    }
}

void changeLightsBoundCallback(int value){
    if(ViewerApplication::currentApplication != nullptr){
        dynamic_cast<ViewerApplication *>(ViewerApplication::currentApplication)->changeLightsBound(value > 0);
    }
}

void changeMaterialCallback(int value){
    if(ViewerApplication::currentApplication != nullptr){
        dynamic_cast<ViewerApplication *>(ViewerApplication::currentApplication)->changeMaterial(material_t(value));
    }
}

void changeProjectionModeCallback(int value){
    if(ViewerApplication::currentApplication != nullptr){
        dynamic_cast<ViewerApplication *>(ViewerApplication::currentApplication)->changeProjectionMode(proj_mode_t(value));
    }
}

void changeReflectionModelCallback(int value){
    if(ViewerApplication::currentApplication != nullptr){
        dynamic_cast<ViewerApplication *>(ViewerApplication::currentApplication)->changeReflectionModel(reflection_model_t(value));
    }
}

void changeShadingModeCallback(int value){
    if(ViewerApplication::currentApplication != nullptr){
        dynamic_cast<ViewerApplication *>(ViewerApplication::currentApplication)->changeShadingMode(shading_mode_t(value));
    }
}

void switchLightOnOffCallback(int value){
    if(ViewerApplication::currentApplication != nullptr){
        dynamic_cast<ViewerApplication *>(ViewerApplication::currentApplication)->switchLightOnOff(GLuint(value));
    }  
}

/* Rotation/zoom constant */
const GLfloat ViewerApplication::ZOOM_DELTA = 0.1;
const GLfloat ViewerApplication::ROTATION_DELTA = 3.0;
const color_t ViewerApplication::DEFAULT_COLOR = color_t::c_white;

/* Default state variables */
const draw_mode_t ViewerApplication::DEFAULT_DRAW_MODE = draw_mode_t::SHADING_MODE;
const shading_mode_t ViewerApplication::DEFAULT_SHADING_MODE = shading_mode_t::GOURAUD_SHADING;
const reflection_model_t ViewerApplication::DEFAULT_REFLECTION_MODEL = reflection_model_t::PHONG_MODEL;
const material_t ViewerApplication::DEFAULT_MATERIAL = material_t::PLASTIC;
const proj_mode_t ViewerApplication::DEFAULT_PROJECTION_MODE = proj_mode_t::PERSPECTIVE_MODE;
const bool ViewerApplication::DEFAULT_LIGHTS_BOUND = true;
const point3 ViewerApplication::POINT_SOURCE_VECTOR = point3(0.0, -1.0, 0.0);
const point3 ViewerApplication::DIRECTIONAL_SOURCE_VECTOR = point3(-1.0, 0.0, 0.0);
const GLfloat ViewerApplication::DEFAULT_POINT_SOURCE_DISTANCE = 2.5;
const GLfloat ViewerApplication::MIN_POINT_SOURCE_DISTANCE = 1.2;
const GLfloat ViewerApplication::POINT_SOURCE_DISTANCE_STEP = 0.1;

/* Shader attribute names */
const char* ViewerApplication::COLOR_ATTRIB_NAME = "Color";
const char* ViewerApplication::DRAW_MODE_ATTRIB_NAME = "DrawMode";
const char* ViewerApplication::SHADING_MODE_ATTRIB_NAME = "ShadingMode";
const char* ViewerApplication::REFLECTION_MODEL_ATTRIB_NAME = "ReflectionModel";
const char* ViewerApplication::LIGHT_MODEL_MATRIX_NAME = "LightModelMatrix";

ViewerApplication::ViewerApplication(const std::string& title, const int width, const int height) :
            Application(title, width, height){}


void ViewerApplication::init(int argc, char** argv){
    Application::init(argc, argv);
    
    /* set colorLocation and initial color value*/
    this->colorLocation = glGetUniformLocation(this->program, COLOR_ATTRIB_NAME);
    this->drawModeLocation = glGetUniformLocation(this->program, DRAW_MODE_ATTRIB_NAME);
    this->shadingModeLocation = glGetUniformLocation(this->program, SHADING_MODE_ATTRIB_NAME);
    this->reflectionModelLocation = glGetUniformLocation(this->program, REFLECTION_MODEL_ATTRIB_NAME);
    this->lightModelMatrixLocation = glGetUniformLocation(this->program, LIGHT_MODEL_MATRIX_NAME);

    this->o_state = new GLObjectState(0.0, 0.0, 0.0, 0.9);
    this->updateModelMatrix();

    this->c_object = new GLOffXObject(this->program, string("assets/shapeX.offx"), string("assets/texture.ppm"));
    
    this->l_state = new GLObjectState(0.0, 0.0, 0.0, 0.9);
    this->updateLightModelMatrix();

    this->camera = new Camera();
    this->camera->moveZ(this->cameraDistance);
    this->updateViewMatrix();

    this->lightingManager = new LightingManager(this->program);
    this->pointSourceId = this->lightingManager->addLightSource(
        LightSource(
            point4(POINT_SOURCE_VECTOR, 1.0),
            LightProperties(
                color4(0.02, 0.02, 0.02, 0.02),
                color4(0.5, 0.5, 0.5, 0.5),
                color4(0.5, 0.5, 0.5, 0.5)
            )
        )
    );
    this->directionalSourceId = this->lightingManager->addLightSource(
        LightSource(
            point4(DIRECTIONAL_SOURCE_VECTOR, 0.0),
            LightProperties(
                color4(0.02, 0.02, 0.02, 0.02),
                color4(0.75, 0.75, 0.75, 0.75),
                color4(0.75, 0.75, 0.75, 0.75)
            )
        )
    );

    this->changeColor(DEFAULT_COLOR);
    this->changeDrawMode(DEFAULT_DRAW_MODE);
    this->changeLightsBound(DEFAULT_LIGHTS_BOUND);
    this->changePointSourceDistance(DEFAULT_POINT_SOURCE_DISTANCE);
    this->changeProjectionMode(DEFAULT_PROJECTION_MODE);
    this->changeReflectionModel(DEFAULT_REFLECTION_MODEL);
    this->changeShadingMode(DEFAULT_SHADING_MODE);

    this->initMenus();
}

/*
    Updates the model matrix.
    Must be called on each object state change.
*/
void ViewerApplication::updateModelMatrix(){
    // Reload model matrix.
    this->modelMatrix = this->o_state->getModelMatrix();
    glUseProgram(program);
    glUniformMatrix4fv(this->modelMatrixLocation, 1, GL_TRUE, this->modelMatrix);
    glUseProgram(0);

    glutPostRedisplay();
}


/* 
    Update the light model matrix.
    Must be called on each light state change.
*/
void ViewerApplication::updateLightModelMatrix(){
    // Reload model matrix.
    this->lightModelMatrix = this->l_state->getModelMatrix();
    glUseProgram(program);
    glUniformMatrix4fv(this->lightModelMatrixLocation, 1, GL_TRUE, this->lightModelMatrix);
    glUseProgram(0);

    glutPostRedisplay();
}

/*
    Updates the view matrix.
    Must be called on each camera change.
*/
void ViewerApplication::updateViewMatrix(){
    if (this->camera != nullptr){
        this->viewMatrix = this->camera->getViewMatrix();
    } else {
        this->viewMatrix = Translate(0.0, 0.0, -1.0 * this->cameraDistance);
    }
    
    glUseProgram(program);
    glUniformMatrix4fv(this->viewMatrixLocation, 1, GL_TRUE, this->viewMatrix);
    glUseProgram(0);

    glutPostRedisplay();
}


/*
    Updates the projection matrix.
    Must be called on changes in clipping volume.
*/
void ViewerApplication::updateProjectionMatrix(){
    GLfloat aspect = ((GLfloat) this->width / (GLfloat) this->height);

    if (this->projectionMode == proj_mode_t::PERSPECTIVE_MODE){
        if(aspect <= 1.0)
            this->projectionMatrix = Frustum( (GLfloat) -0.5,  (GLfloat) 0.5, -0.5 / aspect, 0.5 / aspect, (GLfloat) 2.0, (GLfloat) 20.0);
        else
            this->projectionMatrix = Frustum( -0.5 * aspect, 0.5 * aspect, (GLfloat) -0.5, (GLfloat) 0.5, (GLfloat) 2.0, (GLfloat) 20.0);
    } else if (this->projectionMode == proj_mode_t::ORTHOGRAPHIC_MODE){
        if (aspect <= 1.0)
            this->projectionMatrix = Ortho( -1.0, 1.0, -1.0  / aspect, 1.0 / aspect, this->cameraDistance - 2.0, this-> cameraDistance + 16.0);
        else
            this->projectionMatrix = Ortho( -1.0 * aspect, 1.0 * aspect, -1.0, 1.0, this->cameraDistance - 2.0, this->cameraDistance + 16.0);
    }

    glUseProgram(this->program);
    glUniformMatrix4fv(this->projectionMatrixLocation, 1, GL_TRUE, this->projectionMatrix);
    glUseProgram(0);

    glutPostRedisplay();
}

/* Changes the backgroud color */
void ViewerApplication::changeBackgroudColor(const color_t& color){
    switch(color){
        case c_black   : this->setClearColor(color4( 0.0, 0.0, 0.0, 1.0 )); break;
        case c_gray    : this->setClearColor(color4( 0.5, 0.5, 0.5, 1.0 )); break;
        case c_white   : this->setClearColor(color4( 1.0, 1.0, 1.0, 1.0 )); break;
        default        : break;
    }
    glutPostRedisplay();
}


/* Changes the current color. */
void ViewerApplication::changeColor(const color_t& color){
    this->currentColor = color;
    color4 color_vec;

    switch(this->currentColor)
    {
        case c_black   : color_vec = color4( 0.0, 0.0, 0.0, 1.0 ); break;
        case c_red     : color_vec = color4( 1.0, 0.0, 0.0, 1.0 ); break;
        case c_yellow  : color_vec = color4( 1.0, 1.0, 0.0, 1.0 ); break;
        case c_green   : color_vec = color4( 0.0, 1.0, 0.0, 1.0 ); break;
        case c_blue    : color_vec = color4( 0.0, 0.0, 1.0, 1.0 ); break;
        case c_magenta : color_vec = color4( 1.0, 0.0, 1.0, 1.0 ); break;
        case c_white   : color_vec = color4( 1.0, 1.0, 1.0, 1.0 ); break;
        case c_cyan    : color_vec = color4( 0.0, 1.0, 1.0, 1.0 ); break;
        default        : color_vec = color4( 1.0, 1.0, 1.0, 1.0 ); break;
    }

    glUseProgram(program);
    glUniform4fv(colorLocation, 1, color_vec);
    glUseProgram(0);

    if(this->c_object != nullptr){
        c_object->setColor(color_vec);
    }

    glutPostRedisplay();
}

/* Changes the draw mode */
void ViewerApplication::changeDrawMode(const draw_mode_t& mode){
    glUseProgram(this->program);
    
    if (mode == draw_mode_t::WIREFRAME_MODE){
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glUniform1ui(this->drawModeLocation, (GLuint) draw_mode_t::WIREFRAME_MODE);
    } else if (mode == draw_mode_t::SHADING_MODE){
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glUniform1ui(this->drawModeLocation, (GLuint) draw_mode_t::SHADING_MODE);
    } else if (mode == draw_mode_t::TEXTURE_MODE){
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glUniform1ui(this->drawModeLocation, (GLuint) draw_mode_t::TEXTURE_MODE);
    } else if (mode == draw_mode_t::TEXTURE_SHADING_MODE){
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glUniform1ui(this->drawModeLocation, (GLuint) draw_mode_t::TEXTURE_SHADING_MODE);
    }

    glUseProgram(0);

    glutPostRedisplay();
}

/* Change lights bound mode */
void ViewerApplication::changeLightsBound(const bool is_bound){
    this->lightsBound = is_bound;
}

/* Change material type */
void ViewerApplication::changeMaterial(const material_t& material){
    if(this->c_object !=nullptr){
        switch(material)
        {
            case material_t::PLASTIC   : this->c_object->setShininess(10.0); break;
            case material_t::METALIC   : this->c_object->setShininess(1000.0); break;
            default                    : break;
        }
    }
    glutPostRedisplay();
}

/* Chages the point sources distance */
void ViewerApplication::changePointSourceDistance(const GLfloat& distance){
    this->pointSourceDistance = distance;
    this->lightingManager->setPosition(this->pointSourceId, point4(POINT_SOURCE_VECTOR * this->pointSourceDistance, 1.0));
    glutPostRedisplay();
}


/* Changes the projetion mode */
void ViewerApplication::changeProjectionMode(const proj_mode_t& mode){
    this->projectionMode = mode;
    this->updateProjectionMatrix();
}

/* Changes reflection mode */
void ViewerApplication::changeReflectionModel(const reflection_model_t& model){
    glUseProgram(this->program);
    glUniform1ui(this->reflectionModelLocation, (GLuint) model);
    glUseProgram(0);
    glutPostRedisplay();
}

/* Changes shading mode */
void ViewerApplication::changeShadingMode(const shading_mode_t& mode){
    glUseProgram(this->program);
    glUniform1ui(this->shadingModeLocation, (GLuint) mode);
    glUseProgram(0);
    glutPostRedisplay();
}


/* Light source switch */
void ViewerApplication::switchLightOnOff(const GLuint& source_id){
    if(this->lightingManager->lightEnabled(source_id))
        this->lightingManager->disableLightSource(source_id);
    else
        this->lightingManager->enableLightSource(source_id);

    glutPostRedisplay();
}



/* Initilizes and attaches pop-up menu options */
void ViewerApplication::initMenus(){
    glutSetWindow(this->windowHandle);

    this->changeDrawModeMenuHandle = glutCreateMenu(changeDrawModeCallback);
    glutSetMenu(this->changeDrawModeMenuHandle);
    glutAddMenuEntry("Wireframe", WIREFRAME_MODE);
    glutAddMenuEntry("Shading", SHADING_MODE);
    glutAddMenuEntry("Texture", TEXTURE_MODE);
    glutAddMenuEntry("Texture Shading", TEXTURE_SHADING_MODE);

    this->changeColorMenuHandle = glutCreateMenu(changeColorCallback);
    glutSetMenu(this->changeColorMenuHandle);

    glutAddMenuEntry("Black", c_black);
    glutAddMenuEntry("Red", c_red);
    glutAddMenuEntry("Yellow", c_yellow);
    glutAddMenuEntry("Green", c_green);
    glutAddMenuEntry("Blue", c_blue);
    glutAddMenuEntry("Magenta", c_magenta);
    glutAddMenuEntry("White", c_white);
    glutAddMenuEntry("Cyan", c_cyan);

    this->changeBackgroudColorMenuHandle = glutCreateMenu(changeBackgroudColorCallback);
    glutSetMenu(this->changeProjectionMenuHandle);

    glutAddMenuEntry("White", c_white);
    glutAddMenuEntry("Gray", c_gray);
    glutAddMenuEntry("Black", c_black);

    this->changeProjectionMenuHandle = glutCreateMenu(changeProjectionModeCallback);
    glutSetMenu(this->changeProjectionMenuHandle);

    glutAddMenuEntry("Orthographic", ORTHOGRAPHIC_MODE);
    glutAddMenuEntry("Perspective", PERSPECTIVE_MODE);

    this->changeShadingModeMenuHandle = glutCreateMenu(changeShadingModeCallback);
    glutSetMenu(this->changeShadingModeMenuHandle);

    glutAddMenuEntry("Gouraud", GOURAUD_SHADING);
    glutAddMenuEntry("Phong", PHONG_SHADING);

    this->changeReflectionModelMenuHandle = glutCreateMenu(changeReflectionModelCallback);
    glutSetMenu(this->changeReflectionModelMenuHandle);

    glutAddMenuEntry("Phong", PHONG_MODEL);
    glutAddMenuEntry("Modified Phong", MODIFIED_PHONG_MODEL);

    this->switchLightMenuHandle = glutCreateMenu(switchLightOnOffCallback);
    glutSetMenu(this->switchLightMenuHandle);

    glutAddMenuEntry("Point Source", this->pointSourceId);
    glutAddMenuEntry("Directional Source", this->directionalSourceId);

    this->changeLightsBoundMenuHandle = glutCreateMenu(changeLightsBoundCallback);
    glutSetMenu(this->changeLightsBoundMenuHandle);

    glutAddMenuEntry("Bound", 1);
    glutAddMenuEntry("Unbound", 0);

    this->changeMaterialMenuHandle = glutCreateMenu(changeMaterialCallback);
    glutSetMenu(this->changeMaterialMenuHandle);

    glutAddMenuEntry("Plastic", material_t::PLASTIC);
    glutAddMenuEntry("Metalic", material_t::METALIC);

    this->mainMenuHandle = glutCreateMenu(NULL);
    glutSetMenu(this->mainMenuHandle);
    glutAddSubMenu("Drawing Mode", this->changeDrawModeMenuHandle);
    glutAddSubMenu("Object Color", this->changeColorMenuHandle);
    glutAddSubMenu("Backgroud Color", this->changeBackgroudColorMenuHandle);
    glutAddSubMenu("Projection Mode", this->changeProjectionMenuHandle);
    glutAddSubMenu("Shading Mode", this->changeShadingModeMenuHandle);
    glutAddSubMenu("Reflection Mode", this->changeReflectionModelMenuHandle);
    glutAddSubMenu("Light Switch", this->switchLightMenuHandle);
    glutAddSubMenu("Light Bound", this->changeLightsBoundMenuHandle);
    glutAddSubMenu("Material", this->changeMaterialMenuHandle);

    glutAttachMenu(GLUT_RIGHT_BUTTON);
}

/* Keyboard controls. */
void ViewerApplication::keyControl(unsigned char key, int x, int y){
    switch (key)
    {
        case 'd':
            {
                this->changePointSourceDistance(this->pointSourceDistance + POINT_SOURCE_DISTANCE_STEP);
                break;
            }
        case 'D':
            {
                GLfloat distance = this->pointSourceDistance - POINT_SOURCE_DISTANCE_STEP;
                if(distance >= MIN_POINT_SOURCE_DISTANCE){
                    this->changePointSourceDistance(distance);
                }
                break;
            }
        case 'k': case 'K':
            {
                this->o_state->rotateY(ROTATION_DELTA);
                this->updateModelMatrix();
                if(this->lightsBound){
                    this->l_state->rotateY(ROTATION_DELTA);
                    this->updateLightModelMatrix();
                }
                break;
            }
        case 'l': case 'L':
            {
                this->o_state->rotateY(-1.0 * ROTATION_DELTA);
                this->updateModelMatrix();
                if(this->lightsBound){
                    this->l_state->rotateY(-1.0 * ROTATION_DELTA);
                    this->updateLightModelMatrix();
                }
                break;
            }
        case 'z':
            {
                this->o_state->zoomIn(ZOOM_DELTA);
                this->updateModelMatrix();
                this->l_state->zoomIn(ZOOM_DELTA);
                this->updateLightModelMatrix();
                break;
            }
        case 'Z':
            {
                this->o_state->zoomOut(ZOOM_DELTA);
                this->updateModelMatrix();
                this->l_state->zoomOut(ZOOM_DELTA);
                this->updateLightModelMatrix();
                break;
            }
        case 'i':
            {
                this->o_state->resetRotation();
                this->updateModelMatrix();
                if(this->lightsBound){
                    this->l_state->resetRotation();
                    this->updateLightModelMatrix();
                }
                break;
            }
        case 'h':
            {
                cout << endl << "### HELP ###" << endl
                    << "Arrow key  (see below) -- set the object rotation around X-axis (up and down arrow) and around Z-axis (left and right arrow)" << endl
                    << "k/l -- to turn object around Y-axis" << endl
                    << "i -- initialize rotation" << endl
                    << "z -- zoom-in, and Z -- zoom-out" << endl
                    << "d/D to adjust distance of the point light source" << endl
                    << "Right-Click to see for menu options" << endl
                    << "h -- help, print explanation of your input commands (simply to command line)" << endl
                    << "q -- quit (exit) the program" << endl;
                break;
            }
        case 'q':
            {
                glutLeaveMainLoop();
                break;
            }
        default:
            {
                break;
            }
    }
}


/* Renders the current object on the focus. */
void ViewerApplication::render(){
    // The rendering function.
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Any drawing should be done here
    if (c_object != nullptr){
        if (!c_object->isAllocated()){
            c_object->allocate();
        }
        c_object->draw();
    }

    glutSwapBuffers();
}

/* Readjust viewport on window size changes. Updates projection matrix. */
void ViewerApplication::resize(int width, int height){
    this->width = width;
    this->height = height;
    glViewport(0, 0, width, height);
    this->updateProjectionMatrix();
}

/* Arrow controls. */
void ViewerApplication::specialKeyControl(int key, int x, int y){
    switch (key){
        case GLUT_KEY_LEFT:
        {
            this->o_state->rotateZ(ROTATION_DELTA);
            this->updateModelMatrix();
            if(this->lightsBound){
                this->l_state->rotateZ(ROTATION_DELTA);
                this->updateLightModelMatrix();
            }
            break;
        }
        case GLUT_KEY_RIGHT:
        {
            this->o_state->rotateZ(-1.0 * ROTATION_DELTA);
            this->updateModelMatrix();
            if(this->lightsBound){
                this->l_state->rotateZ(-1.0 * ROTATION_DELTA);
                this->updateLightModelMatrix();
            }
            break;
        }
        case GLUT_KEY_DOWN:
        {
            this->o_state->rotateX(ROTATION_DELTA);
            this->updateModelMatrix();
            if(this->lightsBound){
                this->l_state->rotateX(ROTATION_DELTA);
                this->updateLightModelMatrix();
            }
            break;
        }
        case GLUT_KEY_UP:
        {
            this->o_state->rotateX(-1.0 * ROTATION_DELTA);
            this->updateModelMatrix();
            if(this->lightsBound){
                this->l_state->rotateX(-1.0 * ROTATION_DELTA);
                this->updateLightModelMatrix();
            }
            break;
        }
        default:
        {
            break;
        }
    }
}