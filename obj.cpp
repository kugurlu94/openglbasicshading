#include <vector>
#include "obj.h"


/*
    GLObjectState class implementation.
    Class is used for storing the rotation and scale information
    of an object. And used for generating the ModelView matrix for
    object in its current state.   
*/
const GLfloat GLObjectState::DEFAULT_X = 0.0;
const GLfloat GLObjectState::DEFAULT_Y = 0.0;
const GLfloat GLObjectState::DEFAULT_Z = 0.0;
const GLfloat GLObjectState::DEFAULT_SCALE = 1.0;
const GLfloat GLObjectState::MIN_SCALE = 0.2;
const GLfloat GLObjectState::MAX_SCALE = 2.0;

// Custom constructor.
GLObjectState::GLObjectState(const GLfloat& x_rot, const GLfloat& y_rot, const GLfloat& z_rot, const GLfloat& scale){
    this->init_x = x_rot;
    this->init_y = y_rot;
    this->init_z = z_rot;
    this->init_scale = scale;
    this->rotateMatrix = RotateX(x_rot) * RotateY(y_rot) * RotateZ(z_rot);
    this->scale = scale;
}

/*
    Rotate and zoom methods implemented straightforwardly.
*/
void GLObjectState::rotateX(const GLfloat& rot){
    this->rotateMatrix = RotateX(rot) * this->rotateMatrix;
}

void GLObjectState::rotateY(const GLfloat& rot){
    this->rotateMatrix = RotateY(rot) * this->rotateMatrix;
}

void GLObjectState::rotateZ(const GLfloat& rot){
    this->rotateMatrix = RotateZ(rot) * this->rotateMatrix;
}

void GLObjectState::zoomIn(const GLfloat& val){
    GLfloat s_val = this->scale * (1 + val);
    if(s_val <= MAX_SCALE)
        this->scale = s_val;
}

void GLObjectState::zoomOut(const GLfloat& val){
    GLfloat s_val = this->scale * (1 - val);
    if(s_val >= MIN_SCALE)
        this->scale = s_val;
}

// reset method for reseting the object to its defualt position.
void GLObjectState::reset(){
    this->resetRotation();
    this->scale = this->init_scale;
}

// resetRotation method resets only rotation, leaving scale constant.
void GLObjectState::resetRotation(){
    this->rotateMatrix = RotateX(this->init_x) * RotateY(this->init_y) * RotateZ(this->init_z);
}


mat4 GLObjectState::getModelMatrix(){
    return Scale(this->scale, this->scale, this->scale) * this->rotateMatrix;
}

/*
    abstract GLObject class and its subclasses.
    Used for capsulating a objects properties,
    allocating memory from gpu and drawing.
*/

/* Static shader related members */
const char* GLObject::VERTEX_POSITION_ATTRIB_NAME = "vPosition";
const char* GLObject::TEXTURE_COORDINATE_ATTRIB_NAME = "vTexCoord";
const char* GLObject::NORMAL_POSITION_ATTRIB_NAME = "Normal";

GLObject::GLObject(GLuint program){
    this->program = program;
    this->allocated = false;
    this->positionAttrib = glGetAttribLocation(this->program, VERTEX_POSITION_ATTRIB_NAME);
    this->texCoordAttrib = glGetAttribLocation(this->program, TEXTURE_COORDINATE_ATTRIB_NAME);
    this->normalAttrib = glGetAttribLocation(this->program, NORMAL_POSITION_ATTRIB_NAME);
    this->setReflectivityAttribLocations();
}

void GLObject::setReflectivityAttribLocations(){
    this->ambientAttribLocation = glGetUniformLocation(this->program, "material.ambient");
    this->diffuseAttribLocation = glGetUniformLocation(this->program, "material.diffuse");
    this->specularAttribLocation = glGetUniformLocation(this->program, "material.specular");
    this->shininessAttribLocation = glGetUniformLocation(this->program, "material.shininess");
}

void GLObject::setReflectivityAttribs(){
    glUseProgram(this->program);
    glUniform4fv(this->ambientAttribLocation, 1, this->reflectivityProperties.getAmbient());
    glUniform4fv(this->diffuseAttribLocation, 1, this->reflectivityProperties.getDiffuse());
    glUniform4fv(this->specularAttribLocation, 1, this->reflectivityProperties.getSpecular());
    glUniform1f(this->shininessAttribLocation, this->reflectivityProperties.getShininess());
    glUseProgram(0);
}

bool GLObject::isAllocated(){
    return this->allocated;
}

void GLObject::setReflectivityProperties(const ReflectivityProperties& r_properties){
    this->reflectivityProperties = r_properties;
}

void GLObject::setColor(const color4& color){
    this->setReflectivityProperties(ReflectivityProperties(color, color * 0.75, color * 0.75, this->reflectivityProperties.getShininess()));
}

void GLObject::setShininess(const GLfloat& shininess){
    this->reflectivityProperties.setShininess(shininess);
}

/*
    A simple unit cube implementation, placed in the origin.
    Vertices and indices are defualt member variables defined in header file.
    Just implemeted in order to debug easier.
    Uses element array buffer to draw the cube.
*/
GLCube::GLCube(GLuint program): GLObject(program){
    this->vao_id = 0;
    this->vbo_ids[0] = 0;
    this->vbo_ids[1] = 0;
}


void GLCube::allocate(){
    glUseProgram(program);
    // Create an initilize vertex array object
    if(vao_id == 0)
        glGenVertexArrays(1, &vao_id);

    glBindVertexArray(vao_id);
    // Create and initialize a buffer object;
    // First index for vertex objects, second for index objects.
    if(vbo_ids[0] == 0 && vbo_ids[1] == 0)
        glGenBuffers(2, &vbo_ids[0]);

    glBindBuffer(GL_ARRAY_BUFFER, this->vbo_ids[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // set up vertex arrays;
    glEnableVertexAttribArray(this->positionAttrib);
    glVertexAttribPointer(this->positionAttrib, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

    // set up index buffer object.
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_ids[1]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    glBindVertexArray(0);
    glUseProgram(0);

    this->allocated = true;
}


void GLCube::draw(){
    if(vao_id != 0){
        glUseProgram(program);
        glBindVertexArray(vao_id);

        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, (GLvoid*)0);

        glBindVertexArray(0);
        glUseProgram(0);
    }
}


/*
    Implementation of GLOffObject.
*/

// Utility string tokenizer method for tokenizing the offfile data.
vector<string>& tokenize(vector<string>& result, string& str, string& delimiters){
    // Retuns a vector of strings, tokenized from str using chars in the delimiters.
    result.clear();
    size_t current;
    size_t next = -1;
    do
    {
        current = str.find_first_not_of(delimiters, next + 1);
        if (current == string::npos)
            break;
        next = str.find_first_of(delimiters, current);
        result.push_back(str.substr(current, next - current));
    } while (next != string::npos);

    return result;
}



GLOffObject::GLOffObject(GLuint program, string filename) : GLObject(program) {
    this->vao_id = 0;
    this->vbo_ids[0] = 0;
    this->vbo_ids[1] = 0;

    // initials in the case parsing file fails.
    this->numVertices = 0;
    this->vertices = NULL;

    this->numIndices = 0;
    this->indices = NULL;

    FILE* fp = fopen(filename.c_str(), "r");

    if ( fp != NULL )
    {   
        cout << "Reading from OFF file " << filename << endl;
        // Accessed the file, read and parse.
        fseek(fp, 0L, SEEK_END);
        long size = ftell(fp);

        fseek(fp, 0L, SEEK_SET);
        char* buf = new char[size + 1];
        fread(buf, 1, size, fp);

        buf[size] = '\0';
        fclose(fp);
        
        // Tokenize the buffered file.
        string str(buf);
        string delimiters(" \n\t\r");
        vector<string> tokenized;
        tokenized = tokenize(tokenized, str, delimiters);

        size_t num_tokens = tokenized.size();

        // Parse tokenized string.
        if (tokenized.size() > 3 && tokenized[0] == "OFF"){
            int num_vertices = stoi(tokenized[1]);

            vector<point4> * vertex_vector = new vector<point4>();

            int i;
            for(i=0; i < num_vertices; i++){
                GLfloat x_val = stof(tokenized[3*i+4]);
                GLfloat y_val = stof(tokenized[3*i+5]);
                GLfloat z_val = stof(tokenized[3*i+6]);
                vertex_vector->push_back(point4(x_val, y_val, z_val, 1));
            }

            vector<GLuint> * indices_vector = new vector<GLuint>();

            unsigned int offset = 4 + 3*num_vertices;

            while(offset < num_tokens){
                int num_vert_on_face = stoi(tokenized[offset]);
                int num_triangles = num_vert_on_face - 2;
                int face_indices[num_vert_on_face];
                
                for(i = 0; i < num_vert_on_face; i++)
                    face_indices[i] = stoi(tokenized[offset + i + 1]);
    
                // generate a triagles the face.
                for(i = 0; i < num_triangles; i++){
                    indices_vector->push_back(face_indices[0]);
                    indices_vector->push_back(face_indices[i + 1]);
                    indices_vector->push_back(face_indices[i + 2]);
                }

                offset += num_vert_on_face + 1;
            }

            this->numVertices = num_vertices;
            this->numIndices = indices_vector->size();
            this->vertices = vertex_vector->data();
            this->indices = indices_vector->data();
        }
        else
            cout << "Invalid file header." << endl;
    }
    else
        cout << "Can't read OFF file." << endl;
}

void GLOffObject::allocate(){
    if (numVertices > 0 && numIndices > 0){
        glUseProgram(program);

        // Create an initilize vertex array object
        if(vao_id == 0)
            glGenVertexArrays(1, &vao_id);
        glBindVertexArray(vao_id);

        // Create and initialize a buffer object;
        // First index for vertex objects, second for index objects.
        if((vbo_ids[0] == 0) && (vbo_ids[1] == 0))
            glGenBuffers(2, &vbo_ids[0]);

        glBindBuffer(GL_ARRAY_BUFFER, vbo_ids[0]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices[0]) * numVertices, vertices, GL_STATIC_DRAW);

        // set up vertex arrays;
        glEnableVertexAttribArray(this->positionAttrib);
        glVertexAttribPointer(this->positionAttrib, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

        // set up index buffer objects.
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_ids[1]);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0]) * numIndices, indices, GL_STATIC_DRAW);

        glBindVertexArray(0);
        glUseProgram(0);
    }
    this->allocated = true;
}

void GLOffObject::draw(){
    if(vao_id != 0){
        glUseProgram(program);
        glBindVertexArray(vao_id);

        glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, (GLvoid*)0);

        glBindVertexArray(0);
        glUseProgram(0);
    }
}


GLOffXObject::GLOffXObject(GLuint program, string filename, string textureFilename) : GLObject(program) {
    this->vao_id = 0;
    this->vbo_ids[0] = 0;
    this->vbo_ids[1] = 0;
    this->tex_id = 0;

    // initials
    this->numVertices = 0;
    this->vertices = NULL;

    this->numNormals = 0;
    this->normals = NULL;

    this->numIndices = 0;
    this->indices = NULL;

    this->textureImage = NULL;

    this->parseOffx(filename);
    this->parseImage(textureFilename);
}

/* Parses offx and sets vertex attributes */
void GLOffXObject::parseOffx(string filename){
    FILE* fp = fopen(filename.c_str(), "r");

    if ( fp != NULL )
    {   
        cout << "Reading from OFFX file " << filename << endl;
        // Accessed the file, read and parse.
        fseek(fp, 0L, SEEK_END);
        long size = ftell(fp);

        fseek(fp, 0L, SEEK_SET);
        char* buf = new char[size + 1];
        fread(buf, 1, size, fp);

        buf[size] = '\0';
        fclose(fp);
        
        // Tokenize the buffered file.
        string str(buf);
        string delimiters(" \n\t\r");
        vector<string> tokenized;
        tokenized = tokenize(tokenized, str, delimiters);

        size_t num_tokens = tokenized.size();

        // Parse tokenized string.
        if (tokenized.size() > 3 && tokenized[0] == "OFFX"){
            int num_vertices = stoi(tokenized[1]);
            unsigned int offset = 4;

            vector<point4> * vertex_vector = new vector<point4>();
            vector<point2> * tex_vector = new vector<point2>();
            vector<point3> * normal_vector = new vector<point3>();
            vector<GLuint> * indices_vector = new vector<GLuint>();

            int i;
            for(i=0; i < num_vertices; i++){
                GLfloat x_val = stof(tokenized[3*i+offset]);
                GLfloat y_val = stof(tokenized[3*i+offset+1]);
                GLfloat z_val = stof(tokenized[3*i+offset+2]);
                vertex_vector->push_back(point4(x_val, y_val, z_val, 1.0));
            }

            offset += 3*num_vertices;

            while(offset < num_tokens && tokenized[offset].compare("vt") != 0){
                int num_vert_on_face = stoi(tokenized[offset]);
                int num_triangles = num_vert_on_face - 2;
                int face_indices[num_vert_on_face];
                
                for(i = 0; i < num_vert_on_face; i++)
                    face_indices[i] = stoi(tokenized[offset + i + 1]);
    
                // generates triagles on the face.
                for(i = 0; i < num_triangles; i++){
                    indices_vector->push_back(face_indices[0]);
                    indices_vector->push_back(face_indices[i + 1]);
                    indices_vector->push_back(face_indices[i + 2]);
                }

                offset += num_vert_on_face + 1;
            }

            while(offset < num_tokens && tokenized[offset].compare("vt") == 0){
                GLfloat x_val = stof(tokenized[offset + 1]);
                GLfloat y_val = stof(tokenized[offset + 2]);
                tex_vector->push_back(point2(x_val, y_val));
                offset += 3;
            }

            while(offset < num_tokens && tokenized[offset].compare("vn") == 0){
                GLfloat x_val = stof(tokenized[offset + 1]);
                GLfloat y_val = stof(tokenized[offset + 2]);
                GLfloat z_val = stof(tokenized[offset + 3]);
                normal_vector->push_back(point3(x_val, y_val, z_val));
                offset += 4;
            }

            this->numVertices = num_vertices;
            this->numTexCoords = tex_vector->size();
            this->numNormals = normal_vector->size();
            this->numIndices = indices_vector->size();
            this->vertices = vertex_vector->data();
            this->texCoords = tex_vector->data();
            this->normals = normal_vector->data();
            this->indices = indices_vector->data();
            cout << "Successfully loaded OFFX model." << endl;
        }
        else{
            cout << "Invalid OFFX file header." << endl;
            exit(2);
        }
    }
    else{
        cout << "Can't read OFFX file." << endl;
        exit(2);
    }
}

/* Parse the ppm file and set image properties. */
void GLOffXObject::parseImage(string imageFilename){
    /* Read image file */
    FILE* fp = fopen(imageFilename.c_str(), "r");

    if ( fp != NULL ){
        cout << "Reading from PPM file " << imageFilename << endl;
        fseek(fp, 0L, SEEK_END);
        long size = ftell(fp);

        fseek(fp, 0L, SEEK_SET);
        char* buf = new char[size + 1];
        fread(buf, 1, size, fp);

        buf[size] = '\0';
        fclose(fp);
        
        // Tokenize the buffered file.
        string str(buf);
        string delimiters("\n\r");
        vector<string> tokenized;
        tokenized = tokenize(tokenized, str, delimiters);

        size_t num_tokens = tokenized.size();

        // Parse tokenized string.
        if (tokenized.size() > 1 && tokenized[0] == "P3"){
            unsigned int offset = 1;
            while(offset < num_tokens && tokenized[offset][0] == '#'){
                // skip comments
                offset++;
            }

            string headerDeliminers = " ";
            vector<string> header;
            tokenize(header, tokenized[offset], headerDeliminers);
            this->textureRowLength = stoi(header[0]);
            this->textureColumnLength = stoi(header[1]);
            offset++;

            size_t imageSize = this->textureRowLength * this->textureColumnLength;
            this->textureImage = (GLubyte *) malloc(3 * imageSize * sizeof(GLubyte));

            int rowCounter = 0;
            int columnCounter = 0;
            string lineDelimiters = " ";
            while(offset < num_tokens){
                vector<string> tokens;
                tokenize(tokens, tokenized[offset], lineDelimiters);
                int numTokens = tokens.size();
                int i;
                for(i=0; 3*i + 2 < numTokens; i++){
                    int currentTexel = ((this->textureRowLength - rowCounter - 1) * this->textureColumnLength + columnCounter);
                    this->textureImage[3*currentTexel]     = (GLubyte) stoi(tokens[3*i]);
                    this->textureImage[3*currentTexel + 1] = (GLubyte) stoi(tokens[3*i + 1]);
                    this->textureImage[3*currentTexel + 2] = (GLubyte) stoi(tokens[3*i + 2]);
                    columnCounter++;
                    if(columnCounter == this->textureColumnLength){
                        columnCounter = 0;
                        rowCounter++;
                    }
                }
                offset++;
            }
            cout << "Successfully loaded textures model." << endl;
        }
        else{
            cout << "Invalid PPM file header." << endl;
            exit(2);
        }
    }
    else{
        cout << "Can't read PPM image file." << endl;
        exit(2);
    }
}


void GLOffXObject::allocate(){
    if (numVertices > 0 && numIndices > 0){
        glUseProgram(program);

        // Create an initilize vertex array object
        if(vao_id == 0)
            glGenVertexArrays(1, &vao_id);
        glBindVertexArray(vao_id);

        // Create and initialize a buffer object;
        // First index for vertex objects, second for index objects.
        if((vbo_ids[0] == 0) && (vbo_ids[1] == 0))
            glGenBuffers(2, &vbo_ids[0]);

        size_t size_vertices = sizeof(vertices[0]) * numVertices;
        size_t size_texCoords = sizeof(texCoords[0]) * numTexCoords;
        size_t size_normals = sizeof(normals[0]) * numNormals;

        glBindBuffer(GL_ARRAY_BUFFER, vbo_ids[0]);
        glBufferData(GL_ARRAY_BUFFER, size_vertices + size_texCoords + size_normals, NULL, GL_STATIC_DRAW);
        glBufferSubData(GL_ARRAY_BUFFER, 0, size_vertices, vertices);
        glBufferSubData(GL_ARRAY_BUFFER, size_vertices, size_texCoords, texCoords);
        glBufferSubData(GL_ARRAY_BUFFER, size_vertices + size_texCoords, size_normals, normals);

        // set up vertex attributes;
        glEnableVertexAttribArray(this->positionAttrib);
        glVertexAttribPointer(this->positionAttrib, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

        glEnableVertexAttribArray(this->texCoordAttrib);
        glVertexAttribPointer(this->texCoordAttrib, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(size_vertices));

        glEnableVertexAttribArray(this->normalAttrib);
        glVertexAttribPointer(this->normalAttrib, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(size_vertices + size_texCoords));

        // set up index buffer objects.
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_ids[1]);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0]) * numIndices, indices, GL_STATIC_DRAW);

        glBindVertexArray(0);

        // Load textures.
        glGenTextures(1, &tex_id);

        glBindTexture(GL_TEXTURE_2D, tex_id);
        glTexImage2D(
            GL_TEXTURE_2D, 0, GL_RGB,
            this->textureColumnLength,
            this->textureRowLength,
            0, GL_RGB, GL_UNSIGNED_BYTE,
            this->textureImage);
        glGenerateMipmap(GL_TEXTURE_2D);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); //try here different alternatives
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); //try here different alternatives
        glUseProgram(0);
    }
    this->allocated = true;
}

void GLOffXObject::draw(){
    this->setReflectivityAttribs();
    if(vao_id != 0){
        glUseProgram(program);
        glBindVertexArray(vao_id);
        glBindTexture(GL_TEXTURE_2D, tex_id);

        glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, (GLvoid*)0);

        glBindVertexArray(0);
        glUseProgram(0);
    }
}

