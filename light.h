#ifndef __LIGHT_H
#define __LIGHT_H

#include <Angel.h>
#include <vector>

typedef Angel::vec4 color4;
typedef Angel::vec4 point4;

class LightProperties {
    protected:
        /* Default light components */
        static const color4 DEFAULT_AMBIENT;
        static const color4 DEFAULT_DIFFUSE;
        static const color4 DEFAULT_SPECULAR;

        /* Light components members */
        color4 ambient;
        color4 diffuse;
        color4 specular;

    public:
        /* Explicit constructor */
        explicit LightProperties(
            const color4& ambient_c = DEFAULT_AMBIENT,
            const color4& diffuse_c = DEFAULT_DIFFUSE,
            const color4& specular_c = DEFAULT_SPECULAR
        );

        /* Getters */
        color4& getAmbient();
        color4& getDiffuse();
        color4& getSpecular();
};

class ReflectivityProperties {
    protected:
        /* Default reflectivity components */
        static const color4 DEFAULT_AMBIENT;
        static const color4 DEFAULT_DIFFUSE;
        static const color4 DEFAULT_SPECULAR;
        static const color4 DEFAULT_EMISSION;
        static const GLfloat DEFAULT_SHININESS;

        /* Light components members */
        color4 ambient;
        color4 diffuse;
        color4 specular;

        /* Emission */
        color4 emission;

        /* Shininess */
        GLfloat shininess;

    public:
        /* Explicit constructor */
        explicit ReflectivityProperties(
            const color4& ambient_c = DEFAULT_AMBIENT,
            const color4& diffuse_c = DEFAULT_DIFFUSE,
            const color4& specular_c = DEFAULT_SPECULAR,
            const color4& emission = DEFAULT_EMISSION,
            const GLfloat& shininess = DEFAULT_SHININESS
        );

        /* Getters */
        color4& getAmbient();
        color4& getDiffuse();
        color4& getSpecular();
        color4& getEmission();
        GLfloat& getShininess();

        void setShininess(const GLfloat& mat_shininess);
};

class LightSource {
    protected:
        point4 position;
        LightProperties properties;

    public:
        explicit LightSource(const point4& l_position, const LightProperties& l_properties, const GLfloat& l_distance);
        explicit LightSource(const point4& l_position, const LightProperties& l_properties);
        explicit LightSource(const point4& l_position);
        LightSource();

        /* getters */
        point4& getPosition();
        LightProperties& getProperties();

        /* setters */
        void setPosition(const point4& l_position);
};


class LightingManager {
    protected:
        /* program related */
        GLuint program;
        GLuint enabledLocation;
        GLuint ambientLocation;
        GLuint diffuseLocation;
        GLuint specularLocation;
        GLuint positionLocation;
        GLuint locationOffset;

        vector<LightSource> lightSources;
        vector<bool> lightStatuses;

        void allocateLightSource(const GLuint& program);

    public:
        explicit LightingManager(const GLuint& program);

        /* add a light source */
        GLuint addLightSource(const LightSource& l_source);

        /* enable a light source */
        void enableLightSource(const GLuint& source_id);

        /* disable a light source */
        void disableLightSource(const GLuint& source_id);

        /* get light source enabled info*/
        bool lightEnabled(const GLuint& source_id);

        /* change a light sources position */
        void setPosition(const GLuint& source_id, const point4& position);

};

#endif