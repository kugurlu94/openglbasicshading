#include "camera.h"

Camera::Camera(const point4& eye, const vec4& at, const vec4& up){
    this->eye = eye;
    this->at = at;
    this->up = up;
}

Camera::Camera(const GLfloat& eye_x, const GLfloat& eye_y, const GLfloat& eye_z){
    this->eye = point4(eye_x, eye_y, eye_z, 1.0);
    this->at = vec4(eye_x, eye_y, eye_z - 1.0, 1.0);
    this->up = vec4(0.0, 1.0, 0.0, 0.0);
}

Camera::Camera(){
    this->eye = point4(0.0, 0.0, 0.0, 1.0);
    this->at = point4(0.0, 0.0, -1.0, 1.0);
    this->up = vec4(0.0, 1.0, 0.0, 1.0);
}

void Camera::moveX(const GLfloat& delta_x){
    this->eye.x = this->eye.x + delta_x;
    this->at.x = this->at.x + delta_x;
}
void Camera::moveY(const GLfloat& delta_y){
    this->eye.y = this->eye.y + delta_y;
    this->at.y = this->at.y + delta_y;
}
void Camera::moveZ(const GLfloat& delta_z){
    this->eye.z = this->eye.z + delta_z;
    this->at.z = this->at.z + delta_z;
}
void Camera::move(const vec4& move_vec){
    this->eye += move_vec;
    this->at += move_vec;
}


mat4 Camera::getViewMatrix(){
    return LookAt(this->eye, this->at, this->up);
}