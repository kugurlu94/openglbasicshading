#ifndef __ABSAPP_H
#define __ABSAPP_H

#include <string>
#include <Angel.h>

typedef Angel::vec4 color4;

/* 
    Abstract Application class the user should subclass this class
    and add application specific capabilites.
*/
class Application {
    private:
        /* Callback methods to pass to glut callbacks */
        friend void displayCallback();
        friend void resizeCallback(int width, int height);
        friend void keyboardCallback(unsigned char key, int x, int y);
        friend void specialCallBack(int key, int x, int y);
        friend void mouseCallback(int button, int state, int x, int y);

    protected:
        /* Singleton application pointer */
        static Application * currentApplication;
        
        /* Shader program related static variables. */
        static const char* VERTEX_SHADER;
        static const char* FRAGMENT_SHADER;
        static const char* MODEL_MATRIX_NAME;
        static const char* VIEW_MATRIX_NAME;
        static const char* PROJECTION_MATRIX_NAME;

        /* Window related */
        std::string windowTitle;
        int width, height;
        int windowHandle;

        /* program id*/
        GLuint program;

        /* uniform variable locations */
        GLuint modelMatrixLocation;
        GLuint viewMatrixLocation;
        GLuint projectionMatrixLocation;

        /* trasformation matrices */
        mat4 modelMatrix;
        mat4 viewMatrix;
        mat4 projectionMatrix;

        /* Set initial opengl configurations */
        virtual void glConf();
        void setClearColor(const color4& color);

        /* Methods to update matrices */
        virtual void updateModelMatrix() =0;
        virtual void updateViewMatrix() =0;
        virtual void updateProjectionMatrix() =0;

        /* 
            general utility methods that attaches to callbacks
            Override these methods to specify callback behaviours.
        */
        virtual void keyControl(unsigned char key, int x, int y){};
        virtual void mouseControl(int button, int state, int x, int y){};
        virtual void render(){};
        virtual void resize(int width, int height){};
        virtual void specialKeyControl(int key, int x, int y){};

    public:
        explicit Application(const std::string& title, const int w_width, const int w_height);
        virtual void init(int argc, char ** argv);
        virtual void start();
};
#endif
