#version 450

const uint MAX_LIGHT_SOURCES = 2;

in  vec4 vPosition;
in  vec3 Normal;
in  vec2 vTexCoord;

out vec2 texCoord;
out vec4 shadedColor;

/* phong shading attributes */
out vec3 phongN;
out vec3 phongE;
out vec3 phongL[MAX_LIGHT_SOURCES];

struct LightSource{
    uint enabled;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    vec4 position;
};

struct Material {
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float shininess;
};

uniform uint DrawMode; // Draw mode 0 for wireframe, 1 for shading, 2 for texture
uniform uint ShadingMode; // Shading mode 0 for gouraud, 1 for phong
uniform uint ReflectionModel; // Reflection mode 0 for phong, 1 for modified phong
uniform vec4 Color;
uniform mat4 ModelMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;

uniform mat4 LightModelMatrix; // Model matrix for lights.

uniform LightSource lightSources[MAX_LIGHT_SOURCES];
uniform Material material;

void main() 
{
    // Transform vertex  position into eye coordinates
    vec4 eye_cord = ViewMatrix * ModelMatrix * vPosition;

    if (DrawMode == 1 || DrawMode == 3){ // Shading mode
        shadedColor = vec4(0.0, 0.0, 0.0, 1.0);
        vec3 eye_cord3 = eye_cord.xyz;

        // Transform vertex normal into eye coordinates
        vec3 N = ( ViewMatrix * ModelMatrix * vec4(Normal, 0.0) ).xyz;
        vec3 E = -eye_cord3; // viewer direction

        if (ShadingMode == 1){ // phong shading
            phongN = N;
            phongE = E;

            uint i;
            for(i=0; i < MAX_LIGHT_SOURCES; i++){
                if(lightSources[i].enabled > 0){
                    vec3 L;
                    // light position.w > 0 means a point source
                    if (lightSources[i].position.w > 0){
                        vec3 light_pos3 = (ViewMatrix * LightModelMatrix * lightSources[i].position).xyz;
                        L = light_pos3 - eye_cord3;
                    } else {
                        L = -(LightModelMatrix * lightSources[i].position).xyz;
                    }
                    phongL[i] = L;
                }
            }
        } else {
            vec4 mat_ambient;
            vec4 mat_diffuse;
            vec4 mat_specular;
            float mat_shininess;

            // Override material properties specially for texture shading mode.
            // I chose shades of grey specially to keep colors of the shader.
            if(DrawMode == 3){
                mat_ambient = vec4(0.1, 0.1, 0.1, 1.0);
                mat_diffuse = vec4(1.0, 1.0, 1.0, 1.0);
                mat_specular = vec4(0.6, 0.6, 0.6, 1.0);
                mat_shininess = material.shininess;
            } else {
                mat_ambient = material.ambient;
                mat_diffuse = material.diffuse;
                mat_specular = material.specular;
                mat_shininess = material.shininess;
            }

            // normalize for shading
            N = normalize(N);
            E = normalize(E);

            // Loop adds up color contributions for each light source
            uint i;
            for(i=0; i < MAX_LIGHT_SOURCES; i++){
                if (lightSources[i].enabled > 0){
                    // Compute terms in the illumination equation
                    vec4 ambient = lightSources[i].ambient * mat_ambient;

                    float d_comp = 1.0;
                    vec3 L;
                    // light position.w > 0 means a point source
                    if (lightSources[i].position.w > 0){
                        vec3 light_pos3 = (ViewMatrix * LightModelMatrix * lightSources[i].position).xyz;
                        L = normalize( light_pos3 - eye_cord3 );
                        d_comp = 10 / pow(distance(light_pos3, eye_cord3), 2);
                    } else {
                        L = normalize( -(LightModelMatrix * lightSources[i].position).xyz);
                    }

                    float Kd = d_comp * max( dot(L, N), 0.0 ); //set diffuse to 0 if light is behind the surface point
                    vec4  diffuse = Kd * lightSources[i].diffuse * mat_diffuse;

                    float Ks;
                    if (ReflectionModel == 1){ // modified phong.
                        vec3 H = normalize( L + E ); // halfway vector
                        Ks = d_comp * pow( max(dot(N, H), 0.0), mat_shininess );
                    } else {
                        vec3 R = normalize(2*(dot(L, N)*N) - L);
                        Ks = d_comp * pow( max(dot(R, E), 0.0), mat_shininess );
                    }
                    vec4  specular = Ks * lightSources[i].specular * mat_specular;
                    
                    //ignore also specular component if light is behind the surface point
                    if( dot(L, N) < 0.0 ) {
                        specular = vec4(0.0, 0.0, 0.0, 1.0);
                    }
                    
                    shadedColor += ambient + diffuse + specular;
                }
            }
        }
    } else {
        shadedColor = Color;
    }

    texCoord = vTexCoord;
    gl_Position = ProjectionMatrix * eye_cord;
    
}
